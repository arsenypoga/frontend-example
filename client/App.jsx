import React, { Component } from "react";
import axios from "axios";

const baseUrl = "https://jsonplaceholder.typicode.com";

const Button = ({ onClick }) => (
  <button className="button" onClick={() => onClick()}>
    Click me
  </button>
);

const List = ({ list }) => (
  <div className="list-group panel">
    {list.map((item, key) => (
      <div className="panel-block list-group-item" key={key}>
        <div className="card">
          <header className="card-header">
            <div className="card-header-title">{item.title}</div>
          </header>
          <div className="card-content">{item.body}</div>
        </div>
      </div>
    ))}
  </div>
);

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
    this.getData = this.getData.bind(this);
  }

  getData() {
    axios.get(`${baseUrl}/posts`).then(res => {
      const data = res.data;
      this.setState({ data });
    });
  }

  render() {
    return (
      <div className="container">
        <div className="columns is-centered">
          <div className="column is-half is-narrow">
            <div className="section">
              <div className="content">
                {this.state.data.length === 0 ? (
                  <Button onClick={this.getData} loading={this.state.loading} />
                ) : (
                  <List list={this.state.data} />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

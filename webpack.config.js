const path = require("path");
const webpack = require("webpack");
const FriendlyErrors = require("friendly-errors-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WebpackDashboard = require("webpack-dashboard/plugin");

module.exports = {
  entry: path.resolve(__dirname, "client", "index.js"),
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "bundle.js",
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/, /server/],
        use: ["babel-loader"]
      },
      {
        test: /\.(jpg|jpeg|png|gif|svg)$/,
        exclude: /node_modules/,
        use: ["file-loader"]
      }
    ]
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"]
  },
  devServer: {
    contentBase: path.join(__dirname, "public"),
    hot: true,
    compress: true,
    quiet: true
  },
  mode: "development",
  plugins: [
    new WebpackDashboard(),
    new HtmlWebpackPlugin({ template: __dirname + "/public/index.html" }),
    new FriendlyErrors(),
    new webpack.HotModuleReplacementPlugin()
  ]
};
